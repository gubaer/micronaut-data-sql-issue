@file:Suppress("unused")

package ch.guggis.micronaut.issue

import io.micronaut.context.ApplicationContext
import io.micronaut.data.annotation.MappedEntity
import io.micronaut.data.annotation.Repository
import io.micronaut.data.jdbc.annotation.JdbcRepository
import io.micronaut.data.model.query.builder.sql.Dialect
import io.micronaut.data.repository.CrudRepository
import io.micronaut.runtime.Micronaut
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.GeneratedValue
import jakarta.persistence.Id
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.util.*
import kotlin.system.exitProcess

@Entity
@MappedEntity(value = "messages")
data class Message(
    @Id @GeneratedValue @Column(name = "message_id") val id : Long = 0,
    val message: String,
    @Column(name = "created_at") val createdAt: Date = Date()
)

@Repository(value = "messages")
@JdbcRepository(dialect = Dialect.POSTGRES)
interface MessageRepository: CrudRepository<Message, Long>

object App {
    private val logger: Logger = LoggerFactory.getLogger(App::class.java)

    fun createAndStoreMessage(context: ApplicationContext) {
        val repo = context.getBean(MessageRepository::class.java)

        // get the number of records before we save a new message
        val count1 = repo.count()
        logger.info("Starting. Current number of messages in the database: $count1")
        logger.info("Creating and storing a new message ...")

        // create and save a new message
        var message = Message(id = 0, message = "Hello World!", createdAt = Date())
        message = repo.save(message)
        logger.info("New message: $message")
        logger.info("Creating and storing a new message ... DONE")

        // get the number of records after we've saved a new message
        val count2 = repo.count()

        // after saving, is the number of records raised by 1?
        if (count2 != count1 + 1) {
            throw IllegalStateException("After saving message. Now expected ${count1 + 1} messages, but see $count2")
        } else {
            logger.info("After saving message. Number of messages in the database: $count2, as expected.")

        }

        // after saving, can we look up the new record?
        val retrievedMessage = repo.findById(message.id)
        if (retrievedMessage.isPresent) {
            logger.info("After saving message. Successfully retrieved message with id ${message.id} from database")
        } else {
            throw IllegalStateException("Failed to retrieve message with id ${message.id} from the database")
        }
    }

    @JvmStatic
    fun main(args: Array<String>) {
        val context = Micronaut.run(App::class.java)
        createAndStoreMessage(context)
        exitProcess(0)
    }
}