create table messages (
    message_id serial primary key,
    created_at date not null,
    message VARCHAR(255) not null
);