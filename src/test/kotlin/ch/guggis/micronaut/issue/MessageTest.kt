package ch.guggis.micronaut.issue

import io.micronaut.context.ApplicationContext
import io.micronaut.test.extensions.junit5.annotation.MicronautTest
import jakarta.inject.Inject
import jakarta.inject.Named
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import javax.sql.DataSource

@MicronautTest
class MessageTest01 {

    @Inject
    private lateinit var context: ApplicationContext

    @Test
    fun `can insert a message into the database`() {
        App.createAndStoreMessage(context)
    }
}

@MicronautTest
class MessageTest02 {

    @Inject
    private lateinit var context: ApplicationContext

    @Inject
    @Named("messages")
    private lateinit var ds: DataSource

    @AfterEach
    private fun commit() {
        ds.connection.commit()
    }

    @Test
    fun `can insert a message into the database`() {
        App.createAndStoreMessage(context)
    }
}

@MicronautTest
class MessageTest03 {

    @Inject
    private lateinit var context: ApplicationContext

    @Inject
    @Named("messages")
    private lateinit var ds: DataSource

    private fun commit() {
        ds.connection.commit()
    }

    @Test
    fun `can insert a message into the database`() {
        App.createAndStoreMessage(context)
        commit()
    }
}